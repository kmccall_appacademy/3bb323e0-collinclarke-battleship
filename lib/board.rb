class Board

  DISPLAY_HASH = {
    nil => " ",
    :c => " ",
    :d => " ",
    :b => " ",
    :x => "x",
    :X => "X"
  }

 def self.default_grid
    Array.new(10) { Array.new(10) }
 end

 def self.random
    self.new(self.default_grid, true)
 end

 attr_accessor :default_grid, :grid, :ships

 def initialize(grid = Board.default_grid, random = false)
  @grid = grid
  @ships = []
  random_fleet(5) if random
 end

 def display(shown= false)
   if shown
     DISPLAY_HASH[:b] = "b"
     DISPLAY_HASH[:d] = "d"
     DISPLAY_HASH[:c] = "c"
   else
     DISPLAY_HASH[:b] = " "
     DISPLAY_HASH[:d] = " "
     DISPLAY_HASH[:c] = " "
   end
   header = (0..(grid.length - 1)).to_a.join("  ")
   p "  #{header}"
   grid.each_with_index { |row, i| display_row(row, i) }
 end

 def display_row(row, i)
   chars = row.map { |el| DISPLAY_HASH[el] }.join("  ")
   p "#{i} #{chars}"
 end

 def count
   count = 0
   ships = [:c, :d, :b]
   grid.each{|rows| count += rows.count{|el| ships.include?(el)}}
   count
 end

 def random_fleet(count)
   count.times { place_random_ship }
 end

 def empty?(pos = true)
   if pos == true
     count == 0 ? true : false
   else
     self[pos] == nil ? true : false
   end
 end



 def full?
  count == grid.reduce(:+).count ? true : false
 end

 def [](pos)
   row, col = pos
   grid[row][col]
 end

 def []=(pos, value)
    row, col = pos
    grid[row][col] = value
  end

 def random_pos
   moves = []
   grid.each_with_index do |cols, row|
     cols.each_index do |col|
       next if !empty?([row, col])
       moves << [row, col]
     end
   end
   moves.sample
 end

 def place_random_ship
  raise "board is full" if full?
  placement = random_pos
  self[placement] = :s
 end

 def won?
   count == 0
 end

 def in_range?(pos)
   pos.all? { |x| x.between?(0, grid.length - 1) }
 end

 def place_ship(ship)
  ship.locations.each do |coord|
    self[coord] = ship.mark
  end
  @ships << ship
 end

end
