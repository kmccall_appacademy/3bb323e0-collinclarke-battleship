
require_relative 'board'
require_relative 'player'
require_relative 'computer_player'
require_relative 'ships'

class BattleshipGame

  attr_accessor :current_player, :player1, :player2, :board, :home_harbor, :opponent

  def initialize(player1 = HumanPlayer.new, player2 = ComputerPlayer.new)
    @player1 = player1
    @player2 = player2
    @current_player = player1
    @opponent = player2
    @board = opponent.home_board
    @home_harbor = current_player.home_board
    @hit = false
  end

  def switch!
    @opponent = current_player
    @current_player = current_player == player1 ? player2 : player1
    @board = opponent.home_board
    @home_harbor = current_player.home_board
  end

  def count
    home_harbor.count
  end

  def self.setup
    system "clear"
    puts "welcome to battleship"
    puts "what is your name"
    name1 = gets.chomp
    puts "1 or 2 players"
    input = gets.chomp.to_i
    if input == 1
      BattleshipGame.new(HumanPlayer.new(name1)).play
    else
      puts "what's the other players name"
      name2 = gets.chomp
      BattleshipGame.new(HumanPlayer.new(name1), HumanPlayer.new(name2)).play
    end
  end

  def game_over?
    board.won?
  end

  def attack(pos, mark)
    board[pos] = mark
  end

  def sunk?(ship)
    ship.locations.all?{|coord| board[coord] == :X}
  end

  def play
    place_ships
    switch!
    puts "#{player2.name}'s turn to fill their harbor'"
    place_ships
    switch!
    until game_over?
      play_turn
      switch!
    end
    puts "\n#{current_player.name} sunk all of #{opponent.name}'s ships!!"
    puts "\nplay again? (y/n)"
    input = gets.chomp
    if input == 'y'
      BattleshipGame.setup
    end
  end

  def place_ships
   limit = board.grid.count * 2
   until count >= limit
    ship = nil
    current_player.display_home(self)
    ship = current_player.get_ship
    # debugger
    until ship.locations.all?{|pos| valid_place?(pos)}
      current_player.display_home(self)
      ship = current_player.get_ship
    end
    home_harbor.place_ship(ship)
   end
  end

  def play_turn
    pos = nil
    current_player.display(self)
    until valid_play?(pos)
      pos = current_player.get_play
    end
    @hit = [:c, :d, :b].include?(board[pos])
    mark = @hit ? :X : :x
    attack(pos,  mark)
    report(pos)
  end


  def report(pos)
    if @hit
     ship_attacked = board.ships.select {|ship| ship.locations.include?(pos)}
     puts "\n\n#{current_player.name} aimed perfectly and hammered one home"
     puts "\n* | * * * * * | * \n"
     if sunk?(ship_attacked.first)
       puts "You sunk one of #{opponent.name}'s #{ship_attacked.first.kind}s!"
     else
       puts "one of #{opponent.name}'s ships got hit!"
     end
     puts "* | * * * * *| * \n"
    else
     puts "\n\n\n#{current_player.name} totally whiffed"
    end
  end


  def valid_play?(pos)
   if pos.is_a?(Array) && board.in_range?(pos)
    if pos.length == 2
     board[pos] != :x
    end
   else
    return false
   end
  end

  def valid_place?(pos)
   return false if !pos.is_a?(Array)
    if pos.length == 2 && board.in_range?(pos)
     home_harbor[pos] == nil
    else
     return false
    end
  end

end

if $PROGRAM_NAME == __FILE__
  BattleshipGame.setup
end
