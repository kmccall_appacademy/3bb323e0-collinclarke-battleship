require 'byebug'
class HumanPlayer

  attr_reader :home_board, :name

  def initialize(name = "Anonymous")
    @home_board = Board.new
    @name = name
  end

  def display(game)
    puts "#{game.current_player.name}'s turn\n\n"
    puts "press enter to continue\n\n"
    gets.chomp
    system "clear"
    puts "\n\n\nhome harbor "
    home_board.display(true)
    puts "\n\n\n#{game.opponent.name}'s harbor "
    game.board.display
  end

  def display_home(game)
    system "clear"
    puts "\n\n\n#{name}'s harbor "
    home_board.display(true)
  end

  def get_ship
    key = {
      :c => Ship.cruiser,
      :d => Ship.destroyer,
      :b => Ship.battleship
    }
    puts "\n\n\nenter a coordinate to place a ship"
    input = gets.chomp.split(", ").map {|ch| ch.to_i}
    puts "\n\n pick a ship"
    ship = nil
    until key.keys.include?(ship)
    puts "\nbattleship :b\ndestroyer :d\ncruiser :c"
    ship = gets.chomp.to_sym
    end
    dir = nil
    until [:v, :h].include?(dir)
    puts "Vertical :v or Horizontal :h"
    dir = gets.chomp.to_sym
    end
    ship = key[ship]
    ship.place(input, dir)
    ship
  end

  def get_play
    puts "\n\n\nenter a coordinate to attack"
    gets.chomp.split(", ").map {|ch| ch.to_i}
  end

end
