
class ComputerPlayer

  attr_reader :home_board, :name, :game, :board

  def initialize(name= "coraline")
    @home_board = Board.new
    @name = name
  end

  def display(game)
    @game = game
    @board = game.board
  end

  def display_home(game)
    @home_board = game.home_harbor
  end

  def get_ship
    ship = Ship.random
    coord = get_place
    dir = [:v, :h].sample
    ship.place(coord, dir)
    ship
  end


  def get_play
    ideal_moves = []
    moves = []
    hit, miss = :X, :x
    board.grid.each_with_index do |cols, row|
      cols.each_index do |col|
        pos = [row, col]
        next if board[pos] == :x
        if board[pos] == :X
          adjacents(pos).keys.each do |k|
           ideal_moves << k
          end
        else
         moves << pos
        end
      end
    end

    unless ideal_moves.empty?
      return ideal_moves.sample
    end
    moves.sample
  end

  def adjacents(pos)
    adjacents = Hash.new
    x, y = pos

    adj = [
      [(x-1), (y)],
      [(x+1), (y)],
      [(x), (y+1)],
      [(x), (y-1)]
    ]

    adj_in_range = adj.select{|coord| board.in_range?(coord)}
    adj_in_range.each do |coord|
      adjacents[coord] = board[coord]
    end
    adjacents.select{|k, v| ![:x, :X].include?(v)}
  end

  def get_place
    moves = []
    home_board.grid.each_with_index do |cols, row|
      cols.each_index do |col|
        pos = [row, col]
        next if home_board[pos] != nil
        moves << pos
      end
    end
    moves.sample
  end

end
