class Ship

  attr_reader :size, :locations, :mark, :kind

  def self.cruiser
    Ship.new(2, :c, "Cruiser")
  end

  def self.destroyer
    Ship.new(3, :d, "Destroyer")
  end

  def self.battleship
    Ship.new(5, :b, "Battleship")
  end

  def self.random
    [self.battleship, self.cruiser, self.destroyer].sample
  end

  def initialize(size, mark, kind)
    @size = size
    @mark = mark
    @kind = kind
  end

  def place(pos, dir)
    @locations = []
    x, y = pos
    sections = 0
    case dir
     when :h
       until sections == size
        @locations << [x, y]
        y += 1
        sections += 1
       end
     when :v
       until sections == size
        @locations << [x, y]
        x += 1
        sections += 1
       end
    end
  end



end
